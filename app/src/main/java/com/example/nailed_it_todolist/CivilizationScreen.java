package com.example.nailed_it_todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class CivilizationScreen extends AppCompatActivity {

    // Variable for align text, Button ...
    private ImageView Civ; // image for civilization screen
    private Button Next;
    private Button Prev;
    private TextView swipe ;
    private TextView nameCiv; // title = name of the civilization
    private TextView textCiv;
    int counter = 1; // This counter allows to know what image is currently screened on the device
    // Initialization of design components + set the text of the title and the the page counter
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_civilization_screen);
        Civ = (ImageView) findViewById(R.id.picture3);
        Next = (Button) findViewById(R.id.nextButton);
        Prev = (Button) findViewById(R.id.prevButton);
        swipe = (TextView) findViewById(R.id.swipe1);
        nameCiv = (TextView) findViewById(R.id.nameCiv);
        textCiv = (TextView) findViewById(R.id.textCiv);
        swipe.setText(counter + "/3");
        nameCiv.setText("Egyptian civilization");
        textCiv.setText(""+egyptian);
        Prev.setEnabled(false);
    }
    // This function allows to change image to the right
    public void nextButton(View view) {

        counter++; // Each click, the counter take 1 and the next image comes
        swipe.setText(counter + "/3"); // Set the text of the counter

        if (counter == 1) { // If the user arrived at the last image
            Prev.setEnabled(false); // The next button is blocked
        }else {
            Prev.setEnabled(true);
        }
        // List of pictures present on the application with their name
        switch (counter) {
            case 1:
                Civ.setImageResource(R.drawable.egyptian_civ);
                nameCiv.setText("Egyptian civilization");
                textCiv.setText(""+egyptian);
            case 2:
                Civ.setImageResource(R.drawable.grec_civ);
                nameCiv.setText("Greek civilization");
                textCiv.setText(""+greek);
                break;
            case 3:
                Civ.setImageResource(R.drawable.maya);
                nameCiv.setText("Maya civilization");
                textCiv.setText(""+mayaCiv);
                break;
            default:
                break;
        }
        if (counter == 3) { // If the user arrived at the last image
            Next.setEnabled(false); // The next button is blocked
        }
    }
    // This function allows to change image to the left
    public void prevButton(View view) {
        Next.setEnabled(true); // Block the possibility of click on the Next button
        counter --;// Each click, the counter lose 1 and the next image comes
        swipe.setText(counter + "/3"); // Set the text of the counter
        // List of pictures present on the application with their name
        switch (counter) {
            case 1:
                Civ.setImageResource(R.drawable.egyptian_civ);
                nameCiv.setText("Egyptian civilization");
                textCiv.setText(""+egyptian);
                break;
            case 2:
                Civ.setImageResource(R.drawable.grec_civ);
                nameCiv.setText("Greek civilization");
                textCiv.setText(""+greek);
                break;
            case 3:
                Civ.setImageResource(R.drawable.maya);
                nameCiv.setText("Maya civilization");
                textCiv.setText(""+mayaCiv);
                break;
            default:
                break;
        }
        if (counter == 1) {// If the user arrived at the first image
            Prev.setEnabled(false); // The previous button is blocked
        }
    }
    // Text for Civilization description
    // Egyptian
    String egyptian = "The Egyptian civilization was formed around 4,000 BC after the rise of  writing , and was the most iconic and powerful civilization in history .\n" +
            "\n" +
            "The Egyptian civilization reached a great development in  science ,  art , religion and  commerce . It was noted for the majesty of its monuments covered in hieroglyphics carved on its walls (which are the main sources of information for archaeologists).";
    // Greek
    String greek = "Greek civilization developed in the extreme northeast of the Mediterranean Sea (current territories of Greece and Turkey), and on various islands such as Crete, Cyprus, Rhodes, and Sicily ( Italy ). Around the year 2100 BC the Achaeans, a people with a warrior tradition, invaded and subdued the Cretans, a native people of the island. Thus began to develop the Minoan civilization (the first European civilization to settle on the island of Crete).";
    // Maya
    String mayaCiv ="The Maya civilization (/ˈmaɪə/) was a Mesoamerican civilization developed by the Maya peoples, and noted for its logosyllabic script—the most sophisticated and highly developed writing system in pre-Columbian Americas—as well as for its art, architecture, mathematics, calendar, and astronomical system. The Maya civilization developed in the area that today comprises southeastern Mexico, all of Guatemala and Belize, and the western portions of Honduras and El Salvador.";
}