package com.example.nailed_it_todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class WelcomeScreen extends AppCompatActivity {

    private final int WELCOME_SCREEN_TIMEOUT= 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);
        // Redirect to the "login screen" until 3 seconds
        // Handler post delayed (3 seconds)
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // Start a new screen
                Intent intent = new Intent(WelcomeScreen.this,LoginScreen.class);
                startActivity(intent);
                finish();
            }
        },WELCOME_SCREEN_TIMEOUT);
    }
}