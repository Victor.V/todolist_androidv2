package com.example.nailed_it_todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DinosaursScreen extends AppCompatActivity {

    // Variable for align text, Button ...
    private ImageView Dinosaurs;
    private Button Next;
    private Button Prev;
    private TextView swipe ;
    private TextView nameDino;
    private TextView textDino;
    int counter = 1; // This counter allows to know what image is currently screened on the device

    // Initialization of design components + set the text of the title and the the page counter
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dinosaurs_screen);

        Dinosaurs = (ImageView) findViewById(R.id.picture2);
        Next = (Button) findViewById(R.id.nextButton);
        Prev = (Button) findViewById(R.id.prevButton);
        swipe = (TextView) findViewById(R.id.swipe1);
        nameDino = (TextView) findViewById(R.id.nameCiv);
        textDino = (TextView) findViewById(R.id.textDino);
        swipe.setText(counter + "/3");
        nameDino.setText("Triceratops");
        textDino.setText(""+triceratops);
        Prev.setEnabled(false);
    }
    // This function allows to change image to the right
    public void nextButton(View view) {

        counter++; // Each click, the counter take 1 and the next image comes
        swipe.setText(counter + "/3"); // Set the text of the counter

        if (counter == 1) { // If the user arrived at the last image
            Prev.setEnabled(false); // The next button is blocked
        }else{
            Prev.setEnabled(true);
        }
        // List of pictures present on the application with their name
        switch (counter) {
            case 1:
                Dinosaurs.setImageResource(R.drawable.triceratops);
                nameDino.setText("Triceratops");
                textDino.setText(""+triceratops);
            case 2:
                Dinosaurs.setImageResource(R.drawable.t_rex);
                nameDino.setText("T-Rex");
                textDino.setText(""+tRex);
                break;
            case 3:
                Dinosaurs.setImageResource(R.drawable.mosasaurus);
                nameDino.setText("Mosasaurus");
                textDino.setText(""+mosasaurus);
                break;
            default:
                break;
        }
        if (counter == 3) { // If the user arrived at the last image
            Next.setEnabled(false); // The next button is blocked
        }
    }
    // This function allows to change image to the left
    public void prevButton(View view) {
        Next.setEnabled(true); // Block the possibility of click on the Next button
        counter --;// Each click, the counter lose 1 and the next image comes
        swipe.setText(counter + "/3"); // Set the text of the counter
        // List of pictures present on the application with their name
        switch (counter) {
            case 1:
                Dinosaurs.setImageResource(R.drawable.triceratops);
                nameDino.setText("Triceratops");
                textDino.setText(""+triceratops);
                break;
            case 2:
                Dinosaurs.setImageResource(R.drawable.t_rex);
                nameDino.setText("T-Rex");
                textDino.setText(""+tRex);
                break;
            case 3:
                Dinosaurs.setImageResource(R.drawable.mosasaurus);
                nameDino.setText("Mosasaurus");
                textDino.setText(""+mosasaurus);
                break;
            default:
                break;
        }
        if (counter == 1) {// If the user arrived at the first image
            Prev.setEnabled(false); // The previous button is blocked
        }
    }
    // Text for dino description
    // Triceratops
    String triceratops = "The most striking characteristic of both species is their unique facial structure. They had two paired horns sprouting from their forehead, and a single, smaller horn emerging from the snout.\n" +
            "\n" +
            "Behind the paired horns was a flattened, bony frill. Their bodies were stout, and they walked on all four of their limbs. Including their tail, these dinosaurs measured approximately 26 ft. in length.";
    // T-Rex
    String tRex = "Tyrannosaurus rex, whose name means “king of the tyrant lizards,” was built to rule. This dinosaur’s muscular body stretched as long as 40 feet—about the size of a school bus—from its snout to the tip of its powerful tail. Weighing up to eight tons, T. rex stomped headfirst across its territory on two strong legs. These dinosaurs likely preyed on living animals and scavenged carcasses—and sometimes they even ate one another.";
    // Mosasaurus
    String mosasaurus ="The type species was estimated to be 33' (10m) long. Others believe, however, it could grow up to 60 feet long! Mosasaurus had four paddle-like limbs on a long, streamlined body and a long, powerful tail. The large head had huge jaws, up to 4 ft (1.2 m long) with many teeth. The jaws could open about 3 feet (1 m). The lower jaw is loosely hinged to the skull with a moveable joint on each side (behind the teeth). This loose joint let it swallow huge prey. They would have hunted fish, turtles, molluscs, and shellfish. Ammonites have been found bearing mosasaur teeth marks. ";
}