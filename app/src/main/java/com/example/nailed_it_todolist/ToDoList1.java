package com.example.nailed_it_todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import java.util.ArrayList;
import java.util.List;

public class ToDoList1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list1);

        Dialog Popup;
        ListView listView = findViewById(R.id.listHistory);
        List<String> list = new ArrayList<>();
        list.add(" Dinosaurs ");
        list.add(" War history ");
        list.add(" Civilization ");

        Popup = new Dialog(this);
        Popup.setContentView(R.layout.popup_user);
        Popup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Popup.show();



        ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1,list);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0){
                    //Clicked on Dinosaurs
                    startActivity(new Intent(ToDoList1.this,DinosaursScreen.class));
                }else if(i==1){
                    //Clicked on War History
                    startActivity(new Intent(ToDoList1.this,WarHistoryScreen.class));
                }else if(i==2){
                    //Clicked on Antiquity
                    startActivity(new Intent(ToDoList1.this,CivilizationScreen.class));
                }
            }
        });
    }
}