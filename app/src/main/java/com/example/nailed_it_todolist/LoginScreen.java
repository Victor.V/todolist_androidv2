package com.example.nailed_it_todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginScreen extends AppCompatActivity {

    // Variable for align text, Button ...
    private EditText Name;
    private EditText Password;
    private TextView Info;
    Button Login;
    private  int counter = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        // initialization of design components
        Name = (EditText) findViewById(R.id.idName);
        Password = (EditText) findViewById(R.id.makePassword);
        Info = (TextView) findViewById(R.id.infoAttempts);
        Login = (Button) findViewById(R.id.buttonLogin1);
        // Set the number of attempts
        Info.setText("Attempts : 4");
        // Set the action of login when you click on the login button
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Reading the data write by the user
                validate(Name.getText().toString(),Password.getText().toString());
            }
        });
    }
    // Check if the data write by the user is correct
    private void validate (String userName, String userPassword){
        // If it's correct, the user can access the second page
        if (userName.equals("victor") && userPassword.equals("1234")){
            Intent intent = new Intent(LoginScreen.this,ToDoList1.class);
            startActivity(intent);
        }else{
            // If it's wrong, the user have only 3 attempts
            counter --; // decreasing each time the login is wrong
            Info.setText("Last Attempts : " + counter); // Screen the current number
            // The user waste their chance of login, the android application close itself
            if (counter == 0){
                this.finishAffinity();  // Quit android application programmatically
            }
        }
    }
}