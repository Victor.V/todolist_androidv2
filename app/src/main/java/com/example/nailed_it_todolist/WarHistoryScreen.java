package com.example.nailed_it_todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class WarHistoryScreen extends AppCompatActivity {

    // Variable for align text, Button ...
    private ImageView War;
    private Button Next;
    private Button Prev;
    private TextView swipe ;
    private TextView nameWar;
    private TextView textWar;

    int counter = 1; // This counter allows to know what image is currently screened on the device
    // Initialization of design components + set the text of the title and the the page counter
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_war_history_screen);
        War = (ImageView) findViewById(R.id.picture2);
        Next = (Button) findViewById(R.id.nextButton);
        Prev = (Button) findViewById(R.id.prevButton);
        swipe = (TextView) findViewById(R.id.swipe1);
        nameWar = (TextView) findViewById(R.id.nameCiv);
        textWar = (TextView) findViewById(R.id.textWar);
        swipe.setText(counter + "/3");
        nameWar.setText("Troy war");
        textWar.setText(""+troy);
        Prev.setEnabled(false);
    }
    // This function allows to change image to the right
    public void nextButton(View view) {

        counter++; // Each click, the counter take 1 and the next image comes
        swipe.setText(counter + "/3"); // Set the text of the counter

        if (counter == 1) { // If the user arrived at the last image
            Prev.setEnabled(false); // The next button is blocked
        }else {
            Prev.setEnabled(true);
        }
        // List of pictures present on the application with their name
        switch (counter) {
            case 1:
                War.setImageResource(R.drawable.troy_war);
                nameWar.setText("Troy war");
                textWar.setText(""+troy);
            case 2:
                War.setImageResource(R.drawable.napoleon_war);
                nameWar.setText("Napoleon war");
                textWar.setText(""+napoleon);
                break;
            case 3:
                War.setImageResource(R.drawable.japan_mongol);
                nameWar.setText("Mongol japan invasion");
                textWar.setText(""+mongol);
                break;
            default:
                break;
        }
        if (counter == 3) { // If the user arrived at the last image
            Next.setEnabled(false); // The next button is blocked
        }
    }
    // This function allows to change image to the left
    public void prevButton(View view) {
        Next.setEnabled(true); // Block the possibility of click on the Next button
        counter --;// Each click, the counter lose 1 and the next image comes
        swipe.setText(counter + "/3"); // Set the text of the counter
        // List of pictures present on the application with their name
        switch (counter) {
            case 1:
                War.setImageResource(R.drawable.troy_war);
                nameWar.setText("Troy war");
                textWar.setText(""+troy);
                break;
            case 2:
                War.setImageResource(R.drawable.napoleon_war);
                nameWar.setText("Napoleon war");
                textWar.setText(""+napoleon);
                break;
            case 3:
                War.setImageResource(R.drawable.japan_mongol);
                nameWar.setText("Mongol japan invasion");
                textWar.setText(""+mongol);
                break;
            default:
                break;
        }
        if (counter == 1) {// If the user arrived at the first image
            Prev.setEnabled(false); // The previous button is blocked
        }
    }
    // Text for War description
    // Troy war
    String troy = "The story opens in the tenth year of the Trojan War, which began when the goddess Aphrodite promised Paris the Trojan the heart of the most beautiful woman in the world, Helen, and then caused Helen to fall in love with him. Paris took Helen away and King Agamemnon invaded from Greece to get her back. Xanthe and Marpessa are orphans who live and serve in Priam's palace in Troy; Xanthe is servant to Andromache, King Hector’s wife, and cares for her son Asyntax, and Marpessa serves as Helen’s maid. Helen and Marpessa are very close, and Marpessa regards the beautiful woman as something like a mother to her.";
    // Napoleon war
    String napoleon = "Napoleonic Wars, series of wars between Napoleonic France and shifting alliances of other European powers that produced a brief French hegemony over most of Europe. Along with the French Revolutionary wars, the Napoleonic Wars constitute a 23-year period of recurrent conflict that concluded only with the Battle of Waterloo and Napoleon’s second abdication on June 22, 1815.";
    // Mongol japan invasion
    String mongol ="Major military efforts were taken by Kublai Khan of the Yuan dynasty in 1274 and 1281 to conquer the Japanese archipelago after the submission of the Korean kingdom of Goryeo to vassaldom. Ultimately a failure, the invasion attempts are of macro-historical importance because they set a limit on Mongol expansion and rank as nation-defining events in the history of Japan.";
}